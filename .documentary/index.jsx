import { repository } from '../package.json'
import { format } from 'url'

/**
 * The footer for documentation.
 */
export const footer = () => {
  const alt = 'artdeco/b'
  const src = 'https://gitlab.com/null&s=100'
  const href = 'https://www.artd.eco'
  const org = 'Art Deco™'
  const year = new Date().getFullYear()
  return [
    (<table>
      <tr>
        <td>
          <img src={src} alt={alt} />
        </td>
        <td>
          © <a href={href}>{org}</a> {year}
        </td>
      </tr>
    </table>),
  ]
}

const PipelineBadge = ({ version = 'master' }) => {
  const r = repository.replace('gitlab:', '')
  const badge = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/badges/${version}/pipeline.svg`
  })
  const commits = format({
    protocol: 'https',
    host: 'gitlab.com',
    pathname: `${r}/-/commits/${version}`
  })
  return (<a href={commits}><img src={badge} alt="Pipeline Badge"/></a>)
}

export default {
  'pipeline-badge': PipelineBadge
}