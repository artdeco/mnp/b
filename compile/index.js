const { _b } = require('./b')

/**
 * d
 * @param {!_b.Config} config Options for the program.
 * @param {boolean} [config.shouldRun=true] A boolean option. Default `true`.
 * @param {string} [config.text] A text to return.
 * @return {Promise<string>}
 */
function b(config) {
  return _b(config)
}

module.exports = b

/* typal types/index.xml namespace */
/**
 * @typedef {_b.Config} Config `＠record` Options for the program.
 * @typedef {Object} _b.Config `＠record` Options for the program.
 * @prop {boolean} [shouldRun=true] A boolean option. Default `true`.
 * @prop {string} [text] A text to return.
 */
