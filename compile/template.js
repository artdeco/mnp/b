const { _b } = require('./b')

/**
 * @methodType {_b.b}
 */
function b(config) {
  return _b(config)
}

module.exports = b

/* typal types/index.xml namespace */
