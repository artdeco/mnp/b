/**
 * @fileoverview
 * @externs
 */

/* typal types/index.xml externs */
/** @const */
var _b = {}
/**
 * Options for the program.
 * @record
 */
_b.Config
/**
 * A boolean option. Default `true`.
 * @type {boolean|undefined}
 */
_b.Config.prototype.shouldRun
/**
 * A text to return.
 * @type {string|undefined}
 */
_b.Config.prototype.text

/* typal types/api.xml externs */
/**
 * d
 * @typedef {function(!_b.Config): !Promise<string>}
 */
_b.b
