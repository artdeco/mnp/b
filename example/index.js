import b from '../src'

(async () => {
  const res = await b({
    text: 'example',
  })
  console.log(res)
})()