## API

The package is available by importing its default function:

```js
import b from '@artdeco/b'
```

%~%

<typedef method="b">types/api.xml</typedef>

<typedef>types/index.xml</typedef>

%EXAMPLE: example, ../src => @artdeco/b%
%FORK example%

%~%